package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Details extends AbsEntity {

    @ManyToOne
    private ActionType actionType;

    @ManyToOne
    private ClientPhoneNumber clientPhoneNumber;

    private long amount;
    private String secondSide;
    private boolean from;

}
