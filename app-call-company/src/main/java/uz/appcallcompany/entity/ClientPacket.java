package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class ClientPacket extends AbsEntity {

    @ManyToOne
    private ClientPhoneNumber clientPhoneNumber;

    @ManyToOne
    private Packet packet;

    private long leftover;
    private Timestamp expireDate;
}
