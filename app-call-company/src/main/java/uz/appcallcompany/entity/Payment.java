package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Payment extends AbsEntity {
      private double paySum;
      @ManyToOne
      private ClientPhoneNumber clientPhoneNumber;
}
