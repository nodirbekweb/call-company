package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class ClientTariffExpenseType extends AbsEntity {

    @ManyToOne
    private Tariff tariff;
    @ManyToOne
    private ExpenseType expenseType;
    @ManyToOne
    private ClientPhoneNumber clientPhoneNumber;
    private long leftover;
    private Timestamp deadLine;




}
