package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class ClientPhoneNumber extends AbsEntity {
    @ManyToOne
    private Client client;

    @ManyToOne
    private PhoneNumber phoneNumber;

    private boolean enabled;

    private double balance;
    @ManyToOne
    private Tariff tariff;
}

