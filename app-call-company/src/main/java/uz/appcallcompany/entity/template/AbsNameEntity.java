package uz.appcallcompany.entity.template;

import lombok.Data;

import javax.annotation.Generated;
import javax.persistence.*;

@Data
@MappedSuperclass
public abstract class AbsNameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String nameUz;
    @Column(unique = true)
    private String nameRu;
    @Column(unique = true)
    private String nameEn;
}
