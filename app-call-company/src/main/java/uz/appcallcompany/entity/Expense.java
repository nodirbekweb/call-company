package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Expense extends AbsEntity {
    @ManyToOne
    private ExpenseType expenseType;
    private double sum;
    private long amount;
    @ManyToOne
    private ClientPhoneNumber clientPhoneNumber;

}
