package uz.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.enums.PersonType;
import uz.appcallcompany.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Tariff extends AbsNameEntity {

    private boolean archive;
    private double price;
    private long mb;
    private Integer sms;
    private Integer minuteIn;
    private Integer minuteOut;
    @Enumerated(EnumType.STRING)
    private PersonType personType;
    @OneToMany(mappedBy = "tariff")
    private List<ClientTariffExpenseType> tariffExpenseTypes;
}
