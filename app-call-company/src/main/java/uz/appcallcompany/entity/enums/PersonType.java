package uz.appcallcompany.entity.enums;

public enum  PersonType {
    JURIDICAL,
    PHYSICAL
}
