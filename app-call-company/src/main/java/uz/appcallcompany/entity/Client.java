package uz.appcallcompany.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.appcallcompany.entity.enums.PersonType;
import uz.appcallcompany.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"passportSerial","passportNumber"})})
public class Client extends AbsEntity {
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    private String middleName;
    @Column(nullable = false)
    private Date birthDate;

    private String passportSerial;
    private String passportNumber;

    @Column(nullable = false)
    private String givenPlace;
    @Column(nullable = false)
    private Date givenDate;
    @Column(nullable = false)
    private Date expireDate;
    @Column(nullable = false)
    private String address;

    @Enumerated(EnumType.STRING)
    private PersonType personType;


}
