package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.*;
import uz.appcallcompany.peyload.ApiResponse;
import uz.appcallcompany.peyload.ReqAction;
import uz.appcallcompany.repository.*;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ActionService {

        @Autowired
        ClientPhoneNumberRepository clientPhoneNumberRepository;
        @Autowired
        ClientPacketRepository clientPacketRepository;
        @Autowired
        PacketRepository packetRepository;
        @Autowired
        ExpenseTypeRepository expenseTypeRepository;
        @Autowired
        DetailsRepository detailsRepository;
        @Autowired
        ClientTariffExpenseTypeRepository clientTariffExpenseTypeRepository;

        public ApiResponse saveAction(ReqAction reqAction){
            Optional<ClientPhoneNumber> optionalClientPhoneNumber = clientPhoneNumberRepository.findById(reqAction.getClientId());
            if (optionalClientPhoneNumber.isPresent()){
                ClientPhoneNumber clientPhoneNumber = optionalClientPhoneNumber.get();
                if (clientPhoneNumber.isEnabled()) {
                    ExpenseType expenseType = expenseTypeRepository.findById(reqAction.getExpenseTypeId()).orElseThrow(() -> new ResourceNotFoundException("getExpenseType"));
                    if (!reqAction.isFrom()) {
                            saveDetails(reqAction,expenseType,clientPhoneNumber);
                            return new ApiResponse("Amal muvaffaqiyatli yakunlandi", true);
                    } else{
                        List<Packet> packetList = packetRepository.findAllByExpenseTypesInAndArchiveFalse(Collections.singletonList(expenseType));
                        List<ClientPacket> clientPackets = clientPacketRepository.findAllByClientPhoneNumberAndExpireDateBeforeAndLeftoverGreaterThanAndPacketInOrderByCreatedAt(clientPhoneNumber, new Timestamp(new Date().getTime()), 0, packetList);
                        long yechilgani = 0;
                        for (ClientPacket clientPacket : clientPackets) {
                            if (clientPacket.getLeftover() > reqAction.getAmount() - yechilgani) {
                                yechilgani += reqAction.getAmount() - yechilgani;
                                clientPacket.setLeftover(clientPacket.getLeftover() - (reqAction.getAmount() - yechilgani));
                                break;
                            } else {
                                yechilgani += clientPacket.getLeftover();
                                clientPacket.setLeftover(0);
                            }
                    }
                    if (yechilgani == reqAction.getAmount()) {
                        clientPacketRepository.saveAll(clientPackets);
                        saveDetails(reqAction,expenseType,clientPhoneNumber);
                        return new ApiResponse("Amal muvaffaqiyatli yakunladi", true);
                    }else {
                       Optional<ClientTariffExpenseType> optionalClientTariffExpenseType = clientTariffExpenseTypeRepository.findByExpenseTypeAndClientPhoneNumberAndTariffAndDeadLineAfterAndLeftoverGreaterThan(
                               expenseType,clientPhoneNumber,clientPhoneNumber.getTariff(),new Timestamp(new Date().getTime()),0);
                       if (optionalClientTariffExpenseType.isPresent()){
                            ClientTariffExpenseType clientTariffExpenseType = optionalClientTariffExpenseType.get();
                            if (clientTariffExpenseType.getLeftover() > (reqAction.getAmount() - yechilgani)){
                                clientTariffExpenseType.setLeftover(clientTariffExpenseType.getLeftover()-(reqAction.getAmount()-yechilgani));
                                yechilgani = reqAction.getAmount();
                            }else {
                                yechilgani+=clientTariffExpenseType.getLeftover();
                                clientTariffExpenseType.setLeftover(0);
                            }
                       }
                    }
                }

                }
                    return new ApiResponse("Raqamingiz bloclangan",false);
            }
                return new ApiResponse("Bunday user mavjud emas",false);
        }
        void saveDetails(ReqAction reqAction,ExpenseType expenseType, ClientPhoneNumber clientPhoneNumber){
            Details details = new Details();
            details.setAmount(reqAction.getAmount());
            details.setActionType(expenseType.getActionType());
            details.setClientPhoneNumber(clientPhoneNumber);
            details.setFrom(reqAction.isFrom());
            details.setSecondSide(reqAction.getSecondSide());
            detailsRepository.save(details);
        }
}
