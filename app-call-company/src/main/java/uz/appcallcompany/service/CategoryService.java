package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.Category;
import uz.appcallcompany.peyload.ReqCategory;
import uz.appcallcompany.repository.CategoryRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    public  boolean addCategory(ReqCategory reqCategory){
        boolean exists = categoryRepository.existsByName(reqCategory.getName());
        if (!exists){
            Category category = new Category();
            category.setName(reqCategory.getName());
            category.setPrice(reqCategory.getPrice());
            categoryRepository.save(category);
            return true;
        }
        return false;
    }

    public boolean editCategory(UUID id, ReqCategory reqCategory) {
        boolean exists = categoryRepository.existsByNameAndIdNot(reqCategory.getName(), id);
        if (!exists){
            Optional<Category> optionalCategory = categoryRepository.findById(id);
            if (optionalCategory.isPresent()){
                Category category = optionalCategory.get();
                category.setName(reqCategory.getName());
                category.setPrice(reqCategory.getPrice());
                categoryRepository.save(category);
                return true;
            }
        }
        return false;
    }
}
