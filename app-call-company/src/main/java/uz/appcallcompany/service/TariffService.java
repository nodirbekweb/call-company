package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.Tariff;
import uz.appcallcompany.peyload.ReqTariff;
import uz.appcallcompany.repository.TariffRepository;

import java.util.Optional;

@Service
public class TariffService {

    @Autowired
    TariffRepository tariffRepository;

    public boolean addTariff(ReqTariff reqTariff){
        boolean exists = tariffRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(reqTariff.getNameUz(), reqTariff.getNameRu(), reqTariff.getNameEn());
        if (!exists){
            Tariff tariff = new Tariff();
            tariff.setNameUz(reqTariff.getNameUz());
            tariff.setNameRu(reqTariff.getNameRu());
            tariff.setNameEn(reqTariff.getNameEn());
            tariff.setArchive(reqTariff.isArchive());
            tariff.setMb(reqTariff.getMb());
            tariff.setMinuteIn(reqTariff.getMinuteIn());
            tariff.setMinuteOut(reqTariff.getMinuteOut());
            tariff.setSms(reqTariff.getSms());
            tariff.setPrice(reqTariff.getPrice());
            tariff.setPersonType(reqTariff.getPersonType());
            tariffRepository.save(tariff);
            return  true;
        }
        return false;
    }

    public boolean editTariff(ReqTariff reqTariff, Integer id) {
        boolean exists = tariffRepository.existsByNameUzEqualsIgnoreCaseAndIdNotOrNameRuEqualsIgnoreCaseAndIdNotOrNameEnEqualsIgnoreCaseAndIdNot(
                reqTariff.getNameUz(), id,
                reqTariff.getNameRu(), id,
                reqTariff.getNameEn(), id);
            if (!exists){
                Optional<Tariff> optionalTariff = tariffRepository.findById(id);
                if (optionalTariff.isPresent()){
                    Tariff tariff = optionalTariff.get();
                    tariff.setNameUz(reqTariff.getNameUz());
                    tariff.setNameRu(reqTariff.getNameRu());
                    tariff.setNameEn(reqTariff.getNameEn());
                    tariff.setArchive(reqTariff.isArchive());
                    tariff.setMb(reqTariff.getMb());
                    tariff.setMinuteIn(reqTariff.getMinuteIn());
                    tariff.setMinuteOut(reqTariff.getMinuteOut());
                    tariff.setSms(reqTariff.getSms());
                    tariff.setPrice(reqTariff.getPrice());
                    tariff.setPersonType(reqTariff.getPersonType());
                    tariffRepository.save(tariff);
                    return true;
                }
            }
            return false;
    }
}

