package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.ExpenseType;
import uz.appcallcompany.entity.Tariff;
import uz.appcallcompany.entity.ClientTariffExpenseType;
import uz.appcallcompany.peyload.ReqTariffExpenseType;
import uz.appcallcompany.repository.ExpenseTypeRepository;
import uz.appcallcompany.repository.TariffExpenseTypeRepository;
import uz.appcallcompany.repository.TariffRepository;

import java.util.UUID;

@Service
public class TariffExpenseTypeService {

    @Autowired
    TariffExpenseTypeRepository tariffExpenseTypeRepository;
    @Autowired
    TariffRepository tariffRepository;
    @Autowired
    ExpenseTypeRepository expenseTypeRepository;

    public boolean addTariffExpenseType(ReqTariffExpenseType reqTariffExpenseType){
        ClientTariffExpenseType tariffExpenseType = new ClientTariffExpenseType();
        Tariff tariff = tariffRepository.findById(reqTariffExpenseType.getTariff()).get();
        ExpenseType expenseType = expenseTypeRepository.findById(reqTariffExpenseType.getExpenseType()).get();
        tariffExpenseType.setTariff(tariff);
        tariffExpenseType.setExpenseType(expenseType);
        tariffExpenseType.setPrice(reqTariffExpenseType.getPrice());
        tariffExpenseTypeRepository.save(tariffExpenseType);
        return true;
    }

    public boolean editTariffExpenseType(UUID id, ReqTariffExpenseType reqTariffExpenseType) {
        ClientTariffExpenseType tariffExpenseType = tariffExpenseTypeRepository.findById(id).get();
        Tariff tariff = tariffRepository.findById(reqTariffExpenseType.getTariff()).get();
        ExpenseType expenseType = expenseTypeRepository.findById(reqTariffExpenseType.getExpenseType()).get();
        tariffExpenseType.setTariff(tariff);
        tariffExpenseType.setExpenseType(expenseType);
        tariffExpenseType.setPrice(reqTariffExpenseType.getPrice());
        tariffExpenseTypeRepository.save(tariffExpenseType);
        return true;
    }
}
