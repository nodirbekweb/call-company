package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.ActionType;
import uz.appcallcompany.peyload.ReqActionType;
import uz.appcallcompany.repository.ActionTypeRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ActionTypeService {
    @Autowired
    ActionTypeRepository actionTypeRepository;

    public boolean addActionType(ReqActionType reqActionType){
        boolean exists = actionTypeRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(
                reqActionType.getNameUz(), reqActionType.getNameRu(), reqActionType.getNameEn());
        if (!exists){
            ActionType actionType = new ActionType();
            actionType.setNameUz(reqActionType.getNameUz());
            actionType.setNameRu(reqActionType.getNameRu());
            actionType.setNameEn(reqActionType.getNameEn());
            actionTypeRepository.save(actionType);
            return true;
        }
        return false;
    }

    public List<ActionType> getActionTypes() {
        return actionTypeRepository.findAll();
    }

    public boolean editActionType(Integer id, ReqActionType reqActionType) {
        boolean exists = actionTypeRepository.existsByNameUzEqualsIgnoreCaseAndIdNotOrNameRuEqualsIgnoreCaseAndIdNotOrNameEnEqualsIgnoreCaseAndIdNot(
                reqActionType.getNameUz(), id,
                reqActionType.getNameRu(), id,
                reqActionType.getNameEn(), id);
        if (!exists){
            Optional<ActionType> optionalActionType = actionTypeRepository.findById(id);
            if (optionalActionType.isPresent()){
                ActionType actionType = optionalActionType.get();
                actionType.setNameUz(reqActionType.getNameUz());
                actionType.setNameRu(reqActionType.getNameRu());
                actionType.setNameEn(reqActionType.getNameEn());
                actionTypeRepository.save(actionType);
                return true;
            }
        }
        return false;
    }
}
