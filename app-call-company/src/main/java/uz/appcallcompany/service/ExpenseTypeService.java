package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.ActionType;
import uz.appcallcompany.entity.Expense;
import uz.appcallcompany.entity.ExpenseType;
import uz.appcallcompany.peyload.ReqExpenseType;
import uz.appcallcompany.repository.ActionTypeRepository;
import uz.appcallcompany.repository.ExpenseTypeRepository;

import java.util.Optional;

@Service
public class ExpenseTypeService {
    @Autowired
    ExpenseTypeRepository expenseTypeRepository;
    @Autowired
    ActionTypeRepository actionTypeRepository;

    public boolean addExpenseType(ReqExpenseType reqExpenseType){
        boolean exists = expenseTypeRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(reqExpenseType.getNameUz(),
                reqExpenseType.getNameRu(), reqExpenseType.getNameEn());
            if (!exists){
                Optional<ActionType> optionalActionType = actionTypeRepository.findById(reqExpenseType.getActionType());
                ExpenseType expenseType = new ExpenseType();
                expenseType.setNameUz(reqExpenseType.getNameUz());
                expenseType.setNameRu(reqExpenseType.getNameRu());
                expenseType.setNameEn(reqExpenseType.getNameEn());
                expenseType.setActionType(optionalActionType.get());
                expenseTypeRepository.save(expenseType);
                return true;
            }
            return false;
    }
    public boolean editExpenseType(ReqExpenseType reqExpenseType, Integer id){
        boolean exists = expenseTypeRepository.existsByNameUzEqualsIgnoreCaseAndIdNotOrNameRuEqualsIgnoreCaseAndIdNotOrNameEnEqualsIgnoreCaseAndIdNot(
                reqExpenseType.getNameUz(), id,
                reqExpenseType.getNameRu(), id,
                reqExpenseType.getNameEn(), id);
        if (!exists){
            Optional<ExpenseType> optionalExpenseType = expenseTypeRepository.findById(id);
            if (optionalExpenseType.isPresent()){
                ExpenseType expenseType = optionalExpenseType.get();
                Optional<ActionType> optionalActionType = actionTypeRepository.findById(reqExpenseType.getActionType());
                expenseType.setNameUz(reqExpenseType.getNameUz());
                expenseType.setNameRu(reqExpenseType.getNameRu());
                expenseType.setNameEn(reqExpenseType.getNameEn());
                expenseType.setActionType(optionalActionType.get());
                expenseTypeRepository.save(expenseType);
                return true;
            }
        }
        return false;
    }
}
