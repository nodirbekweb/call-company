package uz.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.appcallcompany.entity.Client;
import uz.appcallcompany.entity.enums.PersonType;
import uz.appcallcompany.peyload.ReqClient;
import uz.appcallcompany.repository.ClientRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;

    public boolean addClient(ReqClient reqClient) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date expireDate = simpleDateFormat.parse(reqClient.getExpireDate());
        Date birthDate = simpleDateFormat.parse(reqClient.getBirthDate());
        Date givenDate = simpleDateFormat.parse(reqClient.getGivenDate());
        boolean exists = clientRepository.existsByPassportNumberEqualsAndPassportSerialEquals(reqClient.getPassportNumber(), reqClient.getPassportSerial());
        if (!exists){
            Client client = new Client();
            client.setFirstName(reqClient.getFirstName());
            client.setLastName(reqClient.getLastName());
            client.setMiddleName(reqClient.getMiddleName());
            client.setAddress(reqClient.getAddress());
            client.setBirthDate(birthDate);
            client.setExpireDate(expireDate);
            client.setGivenDate(givenDate);
            client.setGivenPlace(reqClient.getGivenPlace());
            client.setPassportNumber(reqClient.getPassportNumber());
            client.setPassportSerial(reqClient.getPassportSerial());
            client.setPersonType(reqClient.getPersonType());
            clientRepository.save(client);
            return true;
        }
        return false;
    }

    public boolean editClient(UUID id, ReqClient reqClient) throws ParseException {
        boolean exists = clientRepository.existsByPassportNumberEqualsAndPassportSerialEqualsAndIdNot(
                reqClient.getPassportNumber(), reqClient.getPassportSerial(), id);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date expireDate = simpleDateFormat.parse(reqClient.getExpireDate());
        Date birthDate = simpleDateFormat.parse(reqClient.getBirthDate());
        Date givenDate = simpleDateFormat.parse(reqClient.getGivenDate());
        if (!exists){
            Optional<Client> optionalClient = clientRepository.findById(id);
            if (optionalClient.isPresent()){
                Client editClient = optionalClient.get();
                editClient.setFirstName(reqClient.getFirstName());
                editClient.setLastName(reqClient.getLastName());
                editClient.setMiddleName(reqClient.getMiddleName());
                editClient.setAddress(reqClient.getAddress());
                editClient.setBirthDate(birthDate);
                editClient.setExpireDate(expireDate);
                editClient.setGivenDate(givenDate);
                editClient.setGivenPlace(reqClient.getGivenPlace());
                editClient.setPassportNumber(reqClient.getPassportNumber());
                editClient.setPassportSerial(reqClient.getPassportSerial());
                editClient.setPersonType(reqClient.getPersonType());
                clientRepository.save(editClient);
                return true;
            }
        }
        return false;
    }
}
