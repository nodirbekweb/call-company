package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.Client;

import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, UUID> {
    boolean existsByPassportNumberEqualsAndPassportSerialEquals(String passportNumber, String passportSerial);
    boolean existsByPassportNumberEqualsAndPassportSerialEqualsAndIdNot(String passportNumber, String passportSerial, UUID id);
}
