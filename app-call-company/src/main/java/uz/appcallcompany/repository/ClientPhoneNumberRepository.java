package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.ClientPhoneNumber;

import java.util.UUID;

public interface ClientPhoneNumberRepository extends JpaRepository<ClientPhoneNumber, UUID> {

}
