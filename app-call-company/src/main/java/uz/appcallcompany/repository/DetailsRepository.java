package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.Details;

import java.util.UUID;

public interface DetailsRepository extends JpaRepository<Details, UUID> {

}
