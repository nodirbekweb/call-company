package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.ClientPacket;
import uz.appcallcompany.entity.ClientPhoneNumber;
import uz.appcallcompany.entity.Packet;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface ClientPacketRepository extends JpaRepository<ClientPacket, UUID> {
    List<ClientPacket> findAllByClientPhoneNumberAndExpireDateBeforeAndLeftoverGreaterThanAndPacketInOrderByCreatedAt(ClientPhoneNumber clientPhoneNumber, Timestamp expireDate, long leftover, Collection<Packet> packet);
}
