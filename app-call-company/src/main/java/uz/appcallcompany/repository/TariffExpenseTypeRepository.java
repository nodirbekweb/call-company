package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.ClientTariffExpenseType;

import java.util.UUID;

public interface TariffExpenseTypeRepository extends JpaRepository<ClientTariffExpenseType, UUID> {

}
