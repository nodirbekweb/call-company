package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.ActionType;

import java.util.UUID;

public interface ActionTypeRepository extends JpaRepository<ActionType, Integer> {
    boolean existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(String nameUz, String nameRu, String nameEn);
    boolean existsByNameUzEqualsIgnoreCaseAndIdNotOrNameRuEqualsIgnoreCaseAndIdNotOrNameEnEqualsIgnoreCaseAndIdNot(String nameUz, Integer id, String nameRu, Integer id2, String nameEn, Integer id3);
}
