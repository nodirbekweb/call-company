package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.ExpenseType;
import uz.appcallcompany.entity.Packet;

import java.util.Collection;
import java.util.List;

public interface PacketRepository extends JpaRepository<Packet, Integer> {
    List<Packet> findAllByExpenseTypesInAndArchiveFalse(List<ExpenseType> expenseTypes);
}
