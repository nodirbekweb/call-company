package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.Expense;

import java.util.UUID;

public interface ExpenseRepository extends JpaRepository<Expense, UUID> {

}
