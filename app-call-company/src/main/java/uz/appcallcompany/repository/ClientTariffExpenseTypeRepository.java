package uz.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.appcallcompany.entity.ClientPhoneNumber;
import uz.appcallcompany.entity.ClientTariffExpenseType;
import uz.appcallcompany.entity.ExpenseType;
import uz.appcallcompany.entity.Tariff;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

public interface ClientTariffExpenseTypeRepository extends JpaRepository<ClientTariffExpenseType, UUID> {
    Optional<ClientTariffExpenseType> findByExpenseTypeAndClientPhoneNumberAndTariffAndDeadLineAfterAndLeftoverGreaterThan(ExpenseType expenseType, ClientPhoneNumber clientPhoneNumber, Tariff tariff, Timestamp deadLine, long leftover);
}
