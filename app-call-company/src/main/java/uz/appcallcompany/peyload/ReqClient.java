package uz.appcallcompany.peyload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.appcallcompany.entity.enums.PersonType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqClient {

    private String firstName;

    private String lastName;
    private String middleName;

    private String birthDate;

    private String passportSerial;
    private String passportNumber;


    private String givenPlace;

    private String givenDate;

    private String expireDate;

    private String address;

    private PersonType personType;

}
