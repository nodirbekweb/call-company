package uz.appcallcompany.peyload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqTariffExpenseType {

    private Integer tariff;
    private Integer expenseType;
    private Integer price;
}
