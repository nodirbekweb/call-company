package uz.appcallcompany.peyload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqExpenseType {

    private String nameUz;
    private String nameRu;
    private String nameEn;
    private Integer actionType;
}
