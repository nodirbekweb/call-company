package uz.appcallcompany.peyload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.appcallcompany.entity.enums.PersonType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqTariff {
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private boolean archive;
    private double price;
    private long mb;
    private Integer sms;
    private Integer minuteIn;
    private Integer minuteOut;
    private PersonType personType;
}
