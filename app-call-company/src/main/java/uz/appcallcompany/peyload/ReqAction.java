package uz.appcallcompany.peyload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqAction {

    private UUID clientId;
    private Integer expenseTypeId;
    private long amount;
    private String secondSide;
    private boolean from;
}
