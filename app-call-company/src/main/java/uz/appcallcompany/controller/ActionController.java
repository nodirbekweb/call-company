package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.appcallcompany.peyload.ReqAction;
import uz.appcallcompany.service.ActionService;

@RestController
@RequestMapping("/action")
public class ActionController {

    @Autowired
    ActionService actionService;
    private HttpEntity<?> addAction(@RequestBody ReqAction reqAction){
        actionService.saveAction(reqAction);
    }
}
