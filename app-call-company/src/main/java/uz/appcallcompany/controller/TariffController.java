package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.appcallcompany.entity.Tariff;
import uz.appcallcompany.peyload.ReqTariff;
import uz.appcallcompany.repository.TariffRepository;
import uz.appcallcompany.service.TariffService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/tariff")
public class TariffController {

    @Autowired
    TariffService tariffService;
    @Autowired
    TariffRepository tariffRepository;
    @PostMapping
    public HttpEntity<Boolean> addTariff(@RequestBody ReqTariff reqTariff){
        boolean addTariff = tariffService.addTariff(reqTariff);
        return ResponseEntity.status(addTariff? HttpStatus.CREATED:HttpStatus.CONFLICT).body(addTariff);
    }

    @GetMapping
    public HttpEntity<List<Tariff>> getTariffs(){
        List<Tariff> tariffList = tariffRepository.findAll();
        return ResponseEntity.ok(tariffList);
    }

    @GetMapping(value = "/{id}")
    public HttpEntity<Tariff> getTariff(@PathVariable Integer id){
        Optional<Tariff> optionalTariff = tariffRepository.findById(id);
        if (optionalTariff.isPresent()){
            return ResponseEntity.ok(optionalTariff.get());
        }else return ResponseEntity.ok(new Tariff());
    }

    @PutMapping(value = "/{id}")
    public HttpEntity<Boolean> editTariff(@PathVariable Integer id, @RequestBody ReqTariff reqTariff){
        boolean editTariff = tariffService.editTariff(reqTariff, id);
        return ResponseEntity.status(editTariff?HttpStatus.CREATED:HttpStatus.CONFLICT).body(editTariff);
    }

    @DeleteMapping(value = "/{id}")
    public HttpEntity<Boolean> deleteTariff(@PathVariable Integer id){
        tariffRepository.deleteById(id);
        return ResponseEntity.ok(true);
    }
}
