package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.appcallcompany.entity.Category;
import uz.appcallcompany.peyload.ReqCategory;
import uz.appcallcompany.repository.CategoryRepository;
import uz.appcallcompany.service.CategoryService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping(value = "/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;
    @Autowired
    CategoryRepository categoryRepository;

    @PostMapping
    public HttpEntity<Boolean> addCategory(@Valid @RequestBody ReqCategory reqCategory){
        boolean addCategory = categoryService.addCategory(reqCategory);
        return ResponseEntity.status(addCategory? HttpStatus.CREATED:HttpStatus.CONFLICT).body(addCategory);
    }
    @GetMapping
    public  HttpEntity<List<Category>> getCategories(){
        return ResponseEntity.ok(categoryRepository.findAll());
    }
    @GetMapping(value = "/{id}")
    public HttpEntity<Category> getCategory(@PathVariable UUID id){
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()){
            Category category = optionalCategory.get();
            return ResponseEntity.ok(category);
        }else return ResponseEntity.ok(new Category());
    }
    @PutMapping(value = "/{id}")
    public HttpEntity<Boolean> editCategory(@PathVariable UUID id, @RequestBody ReqCategory reqCategory){
        boolean editCategory = categoryService.editCategory(id, reqCategory);
        return ResponseEntity.status(editCategory?HttpStatus.CREATED:HttpStatus.CONFLICT).body(editCategory);
    }
    @DeleteMapping(value = "/{id}")
    public HttpEntity<Boolean> deleteCategory(@PathVariable UUID id){
        categoryRepository.deleteById(id);
        return ResponseEntity.ok(true);
    }
}
