package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import uz.appcallcompany.entity.ActionType;
import uz.appcallcompany.peyload.ReqActionType;
import uz.appcallcompany.repository.ActionTypeRepository;
import uz.appcallcompany.service.ActionTypeService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Controller
@RequestMapping(value = "/actionType")
public class ActionTypeController {

    @Autowired
    ActionTypeService actionTypeService;
    @Autowired
    ActionTypeRepository actionTypeRepository;
    @PostMapping
    public HttpEntity<Boolean> addActionType(@Valid @RequestBody ReqActionType reqActionType){
        boolean addActionType = actionTypeService.addActionType(reqActionType);
        return ResponseEntity.status(addActionType? HttpStatus.CREATED:HttpStatus.CONFLICT).body(addActionType);
    }
    @GetMapping
    public HttpEntity<List<ActionType>> actionTypes(){
        List<ActionType> actionTypes = actionTypeService.getActionTypes();
        return ResponseEntity.ok(actionTypes);
    }
    @GetMapping(value = "/{id}")
    public HttpEntity<ActionType> getActionType(@PathVariable Integer id){
        Optional<ActionType> getActionType = actionTypeRepository.findById(id);
        return ResponseEntity.ok(getActionType.get());
    }
    @PutMapping(value = "/{id}")
    public HttpEntity<Boolean> editActionType(@PathVariable Integer id, @RequestBody ReqActionType reqActionType){
        boolean editActionType = actionTypeService.editActionType(id, reqActionType);
        return ResponseEntity.status(editActionType?HttpStatus.CREATED:HttpStatus.CONFLICT).body(editActionType);
    }
    @DeleteMapping(value = "/{id}")
    public HttpEntity<Boolean> deleteActionType(@PathVariable Integer id){
        actionTypeRepository.deleteById(id);
        return ResponseEntity.ok(true);
    }
}
