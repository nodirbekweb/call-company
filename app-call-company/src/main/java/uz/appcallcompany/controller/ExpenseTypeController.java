package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.appcallcompany.entity.ExpenseType;
import uz.appcallcompany.peyload.ReqExpenseType;
import uz.appcallcompany.repository.ExpenseTypeRepository;
import uz.appcallcompany.service.ExpenseTypeService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping(value = "/expenseType")
public class ExpenseTypeController  {
    @Autowired
    ExpenseTypeService expenseTypeService;
    @Autowired
    ExpenseTypeRepository expenseTypeRepository;

    @PostMapping
    public HttpEntity<Boolean> addExpenseType(@RequestBody ReqExpenseType reqExpenseType){
        boolean expenseType = expenseTypeService.addExpenseType(reqExpenseType);
        return ResponseEntity.status(expenseType? HttpStatus.CREATED:HttpStatus.CONFLICT).body(expenseType);
    }
    @GetMapping
    public HttpEntity<List<ExpenseType>> getExpenseTypes(){
       return ResponseEntity.ok( expenseTypeRepository.findAll());
    }
    @GetMapping(value = "/{id}")
    public HttpEntity<ExpenseType> getExpenseType(@PathVariable Integer id){
        Optional<ExpenseType> optionalExpenseType = expenseTypeRepository.findById(id);
        return ResponseEntity.ok(optionalExpenseType.get());
    }
    @PutMapping(value = "/{id}")
    public HttpEntity<Boolean> editExpenseType(@PathVariable Integer id, @RequestBody ReqExpenseType reqExpenseType){
        boolean editExpenseType = expenseTypeService.editExpenseType(reqExpenseType, id);
        if (editExpenseType){
            return ResponseEntity.ok(true);
        }else return ResponseEntity.ok(false);
    }
    @DeleteMapping(value = "/{id}")
    public HttpEntity<Boolean> deleteExpenseType(@PathVariable Integer id){
        expenseTypeRepository.deleteById(id);
        return ResponseEntity.ok(true);
    }
}
