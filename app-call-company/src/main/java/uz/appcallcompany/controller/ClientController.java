package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.appcallcompany.entity.Client;
import uz.appcallcompany.peyload.ReqClient;
import uz.appcallcompany.repository.ClientRepository;
import uz.appcallcompany.service.ClientService;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping(value = "/client")
public class ClientController {

    @Autowired
    ClientService clientService;
    @Autowired
    ClientRepository clientRepository;

    @PostMapping
    public HttpEntity<Boolean> addClient(@RequestBody ReqClient reqClient) throws ParseException {
        boolean addClient = clientService.addClient(reqClient);
        return ResponseEntity.status(addClient? HttpStatus.CREATED:HttpStatus.CONFLICT).body(addClient);
    }
    @GetMapping
    public HttpEntity<List<Client>> clients(){
        return ResponseEntity.ok(clientRepository.findAll());
    }

    @GetMapping(value = "/{id}")
    public HttpEntity<Client> getClient(@PathVariable UUID id){
        Optional<Client> byId = clientRepository.findById(id);
        return ResponseEntity.ok(byId.get());
    }
    @PutMapping(value = "/{id}")
    public HttpEntity<Boolean> editClient(@PathVariable UUID id, @RequestBody ReqClient reqClient) throws ParseException {
        boolean editClient = clientService.editClient(id, reqClient);
        return ResponseEntity.status(editClient?HttpStatus.CREATED:HttpStatus.CONFLICT).body(editClient);
    }


    @DeleteMapping(value = "/{id}")
    public HttpEntity<Boolean> deleteClient(@PathVariable UUID id){
        clientRepository.deleteById(id);
        return ResponseEntity.ok(true);
    }

}
