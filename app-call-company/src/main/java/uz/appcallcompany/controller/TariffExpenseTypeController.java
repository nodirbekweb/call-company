package uz.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.appcallcompany.entity.ClientTariffExpenseType;
import uz.appcallcompany.peyload.ReqTariffExpenseType;
import uz.appcallcompany.repository.TariffExpenseTypeRepository;
import uz.appcallcompany.service.TariffExpenseTypeService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping(value = "/tariffExpenseType")
public class TariffExpenseTypeController {
    @Autowired
    TariffExpenseTypeService tariffExpenseTypeService;
    @Autowired
    TariffExpenseTypeRepository tariffExpenseTypeRepository;
    @PostMapping
    public HttpEntity<Boolean> addExpenseType(@RequestBody ReqTariffExpenseType reqTariffExpenseType){
        boolean addTariffExpenseType = tariffExpenseTypeService.addTariffExpenseType(reqTariffExpenseType);
        return ResponseEntity.status(addTariffExpenseType? HttpStatus.CREATED:HttpStatus.CONFLICT).body(addTariffExpenseType);
    }
    @GetMapping
    public HttpEntity<List<ClientTariffExpenseType>> getTariffExpenseTypes(){
        return ResponseEntity.ok(tariffExpenseTypeRepository.findAll());
    }
    @GetMapping(value = "/{id}")
    public HttpEntity<ClientTariffExpenseType> getTariffExpenseType(@PathVariable UUID id){
        Optional<ClientTariffExpenseType> tariffExpenseType = tariffExpenseTypeRepository.findById(id);
        return ResponseEntity.ok(tariffExpenseType.get());
    }
    @PutMapping(value = "/{id}")
    public HttpEntity<Boolean> editTariffExpenseType(@PathVariable UUID id, @RequestBody ReqTariffExpenseType reqTariffExpenseType){
        boolean editTariffExpenseType = tariffExpenseTypeService.editTariffExpenseType(id, reqTariffExpenseType);
        return ResponseEntity.status(editTariffExpenseType?HttpStatus.CREATED:HttpStatus.CONFLICT).body(editTariffExpenseType);
    }
}
